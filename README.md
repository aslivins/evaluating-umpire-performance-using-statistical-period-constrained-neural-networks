# Evaluating Umpire Performance using Statistical Period-Constrained Neural Networks

This work is my Statistical Science Master's Thesis, where I evaluate MLB umpire performance using publicly available pitch data, under the advisement of Paul A. Parker, from the UCSC Statistics Department. I won an award for this work at the UConn Sports Analytics Symposium, where I presented a poster among other graduate students. To view this project, feel free to view the pre-print thesis paper, UCSAS poster, or the accompanying set of presentation slides. 

## Abstract

This work responds to the growing need for a statistically robust
approach to estimate the called strike zones of MLB umpires, and introduces
a novel metric towards evaluating umpire accuracy. Previous research on this
topic does not provide both the explicit representation of a flexible estimated
strike zone and uncertainty quantification for the estimation. To advance beyond
existing methodologies, we develop a period-constrained random-weight neural
network to predict the probability of a called strike on any location inside and
around the rectangular MLB strike zone. By utilizing polar coordinates, we derive
an explicit form of the contour line, which facilitates the measuring of umpire
accuracy through comparison with the MLB strike zone. Our model provides
competitive out-of-sample prediction error when compared with previously used
models. Bayesian inference is used in model fitting, which permits uncertainty
quantification for the contour line. By employing novel metrics to assess umpire
accuracy, we can also evaluate and compare umpire performance league wide,
inform decisions about crucial game assignments, and contribute to the ongoing
dialogue on fair play in baseball.

**Keywords**: Umpire Performance Metrics, Neural Networks, Bayesian Inference, Uncertainty Quantification

# Author: Adam Slivinsky
# Work: Shiny App for Ump Results
# Date: 12 October 2024

#Clear Data Environment
rm(list=ls(all=TRUE))
set.seed(7) 

#Load Libraries
library(shiny)
library(ggplot2)
library(pracma)

# Function to retreive data
retreive_data <- function(UmpName){
  UmpNum <- which(UmpNames == UmpName)
  out_temp <- NULL
  out_temp$num <- UmpNum
  out_temp$name <- UmpName
  out_temp$data <- readRDS(file_names[UmpNum])
  out_temp$DS <- readRDS(file_names2[UmpNum])
  out_temp$beta <- readRDS(file_names3[UmpNum])
  out_temp$CI <- array(readRDS(file_names4[UmpNum]), dim = c(length(LD_theta), 3, 6))[,,6]
  
  return(out_temp)
}

# Function to plot EUZ and pitches for a specific umpire
plot_EUZ1 <- function(umpData, umpCI, umpName){
  ump_data <- umpData
  #Filter out missing values
  ump_data <- ump_data[which(is.na(ump_data$pfx_x) == FALSE),]
  ump_data <- ump_data[which(is.na(ump_data$pfx_z) == FALSE),]
  
  #Get strikes
  called_strike_data <- ump_data[which(ump_data$description == 'called_strike'),]
  
  #Get balls
  ball_data <- ump_data[which(ump_data$description == 'ball'),]
  
  #Combine called strikes and balls
  data1 <- rbind(called_strike_data, ball_data)
  data1$is_strike <- as.numeric(data1$description == 'called_strike')
  data1$px <- data1$plate_x
  data1$pz <- data1$plate_z
  
  #Normalize pz
  #Adjust for strike zone differences
  for (i in 1:length(data1$sz_bot)){
    data1$pz[i] = 2*((data1$pz[i] - data1$sz_top[i])/ (data1$sz_top[i] - data1$sz_bot[i])) + 3.5
  }
  
  #Center pz
  data1$pz <- data1$pz - 2.5
  called_strike_data <- data1[which(data1$description == 'called_strike'),]
  
  data1$r <- rep(0, length(data1$pz))
  data1$theta <- rep(0, length(data1$pz))
  for(i in 1:length(data1$pz)){
    data1$r[i] <- sqrt((data1$px[i])**2 + (data1$pz[i])**2)
    #First quadrant
    if((sign(data1$px[i]) == 1) && (sign(data1$pz[i]) == 1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i]))
    }
    #Second quadrant
    if((sign(data1$px[i]) == -1) && (sign(data1$pz[i]) == 1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + pi
    }
    #Fourth quadrant
    if((sign(data1$px[i]) == 1) && (sign(data1$pz[i]) == -1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + (2*pi)
    }
    #Third quadrant
    if((sign(data1$px[i]) == -1) && (sign(data1$pz[i]) == -1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + pi
    }
  }
  
  #Randomize ball data (helps for plotting)
  data1 <- data1[sample(1:nrow(data1)),]
  
  #Get Confidence interval bands
  CI_LD_r <- umpCI
  LD_x_m <- (CI_LD_r[,2] * cos(LD_theta))
  LD_x_CIL_m <- (CI_LD_r[,1] * cos(LD_theta))
  LD_x_CIU_m <- (CI_LD_r[,3] * cos(LD_theta))
  LD_y_m <- (CI_LD_r[,2] * sin(LD_theta))
  LD_y_CIL_m <- (CI_LD_r[,1] * sin(LD_theta))
  LD_y_CIU_m <- (CI_LD_r[,3] * sin(LD_theta))
  
  #Best Plot 22 
  p <- ggplot(data1, aes(x = px, y = pz, color = description)) + 
    geom_point(alpha = 0.075, cex=10) + 
    ggtitle(paste0(umpName, ' Estimated Umpire Zone\nand 95% Credible Interval Bands,\nusing 2023 MLB called pitch data')) + 
    xlim(-1.2, 1.2) + ylim(-1.5, 1.5) + 
    coord_fixed() + geom_point(data=data.frame(SZonedf), aes(x=x, y=y), color='black', cex=0.3) + 
    scale_color_manual(values=c('green', 'red')) + xlab('Horizontal distance from center of Strike Zone') + 
    ylab('Vertical distance from center of Strike Zone') + 
    geom_point(data = data.frame(x = LD_x_m,
                                 y = LD_y_m,
                                 description = rep(NA, length(LD_x_m))),
               aes(x = x, y = y), cex = 0.75, color = 'black') + 
    geom_point(data = data.frame(x = LD_x_CIL_m,
                                 y = LD_y_CIL_m,
                                 description = rep(NA, length(LD_x_CIL_m))),
               aes(x = x, y = y), cex = 0.1, color = 'blue') + 
    geom_point(data = data.frame(x = LD_x_CIU_m,
                                 y = LD_y_CIU_m,
                                 description = rep(NA, length(LD_x_CIU_m))),
               aes(x = x, y = y), cex = 0.1, color = 'blue')
  
  return(p)
}

# Plot Ump Departure Scores 
plotDS <- function(umpNum){
  out_temp <- NULL
  #Get correct plotting data
  um = umpNum
  colors <- array(0, length(ind))
  colors[which(ind == um)] <- 1
  #Dataframes for plots 1 and 2
  plotDF1 <- data.frame(x = ind[1:(length(ind)/2)], 
                        y = ump_ds_data[1:(length(ind)/2)],
                        col = factor(colors[1:(length(ind)/2)]))
  plotDF2 <- data.frame(x = ind[(length(ind)/2 + 1):(length(ind))], 
                        y = ump_ds_data[(length(ind)/2 + 1):(length(ind))],
                        col = factor(colors[(length(ind)/2 + 1):(length(ind))]))
  #Plots
  out_temp$plot1 <- ggplot(plotDF1, aes(x=as.numeric(x), y=y,
                      group = x,
                      fill = col)) +
    geom_violin(alpha = 0.5, adjust = 1) + 
    labs(title = '2023 MLB Umpire Departure Scores') + 
    xlab('Data Index (i = 1,..., 47) Ordered by Umpire Name') + 
    ylab('Departure Score') + ylim(0.25,1.25) + 
    scale_fill_manual(values = c('grey', 'blue')) + 
    theme(legend.position = "none")
  out_temp$plot2 <- ggplot(plotDF2, aes(x=as.numeric(x), y=y,
                      group = x,
                      fill = col)) +
    geom_violin(alpha = 0.5, adjust = 1) + 
    labs(title = '2023 MLB Umpire Departure Scores') + 
    xlab('Data Index (i = 1,..., 47) Ordered by Umpire Name') + 
    ylab('Departure Score') + ylim(0.25,1.25) + 
    scale_fill_manual(values = c('grey', 'blue')) + 
    theme(legend.position = "none")
  #Return list of plots
  return(out_temp)
}

#Ump parameters
maxU <- 94
file_names <- paste0('Data/ump_seasons/Ump', seq(1, maxU, by = 1), '_23.RDS')
file_names2 <- paste0('Results/N200/DS_RDS/Ump', seq(1, maxU, by = 1), 'DS.RDS')
file_names3 <- paste0('Results/N200/Beta_RDS/Ump', seq(1, maxU, by = 1), 'Beta.RDS')
file_names4 <- paste0('Results/N200/EUZ_RDS/Ump', seq(1, maxU, by = 1), 'EUZ.RDS')
file_names5 <- paste0('Results/N200/Err_RDS/Ump', seq(1, maxU, by = 1), 'Err.RDS')
UmpNames <- readRDS("Data/ump_seasons/UmpNames.RDS")

#Plotting Parameters
LD_theta <- seq(0, (2*pi), by=0.005)
nodes <- 400
weights <- readRDS('RDS/GLM/weights.rds')
biases <- readRDS('RDS/GLM/biases.rds')

#Model prediction
#Define number of linearly spaced points to predict over
num_points <- 100
pred_mat <- NULL
pred_mat$px <- linspace(-2, 2, num_points)
pred_mat$pz <- linspace(-2.5, 2.5, num_points)
pred_mat$is_strike_pred_L2 = array(0, dim = c(num_points, num_points))
#List to store pairs of linearly spaced points 
new_data <- NULL
new_data$px <- array(0, dim = c(num_points * num_points))
new_data$pz <- array(0, dim = c(num_points * num_points))
new_output_layerPL <- array(0, dim = c(nodes, (num_points * num_points)))
#Construct hidden layer, vector notation
for(n1 in 1:num_points){
  for(n2 in 1:num_points){
    new_data$px[(num_points)*(n1 - 1) + n2] <- pred_mat$px[n1]
    new_data$pz[(num_points)*(n1 - 1) + n2] <- pred_mat$pz[n2]
    
    #Get radius of given point
    temp_r <- abs(sqrt((pred_mat$px[n1]**2) + (pred_mat$pz[n2]**2)))
    
    #Get theta of given point
    #First quadrant
    if((sign(pred_mat$px[n1]) == 1) && (sign(pred_mat$pz[n2]) == 1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1]))
    }
    #Second quadrant
    if((sign(pred_mat$px[n1]) == -1) && (sign(pred_mat$pz[n2]) == 1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1])) + pi
    }
    #Fourth quadrant
    if((sign(pred_mat$px[n1]) == 1) && (sign(pred_mat$pz[n2]) == -1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1])) + (2*pi)
    }
    #Third quadrant
    if((sign(pred_mat$px[n1]) == -1) && (sign(pred_mat$pz[n2]) == -1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1])) + pi
    }
    #Construct hidden layer 
    for(h in 1:nodes){
      ind_temp = h %% (nodes/2)
      if(ind_temp == 0){
        ind_temp = (nodes/2)
      }
      h_temp <- sigmoid(weights[ind_temp,1] * cos(temp_theta + weights[ind_temp,2]) + biases[ind_temp])
      if(h <= (nodes/2)){
        new_output_layerPL[h,((num_points)*(n1 - 1) + n2)] <- h_temp * temp_r
      }
      if(h > (nodes/2)){
        new_output_layerPL[h,((num_points)*(n1 - 1) + n2)] <- h_temp 
      }
    }
  }
}

#Transform MLB Strike zone from Cartesian Space to Polar Coordinates
#Define strike zone dataframe with given MLB strike zone locations
SZonedf <- data.frame(x=c(rep(0.8308,200), rep(-0.8308,200), linspace(-0.8308, 0.8308, 200), linspace(-0.8308, 0.8308, 200)),
                      y = c(linspace(-1.1225, 1.1225, 200), linspace(-1.1225, 1.1225, 200), rep(-1.1225,200), rep(1.1225,200)),
                      r = rep(0,800), theta = rep(0,800))
for(i in 1:length(SZonedf$x)){
  #Get radius of given point
  temp_r <- sqrt((SZonedf$x[i]**2) + (SZonedf$y[i]**2))
  
  #Get theta of given point
  #First quadrant
  if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == 1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i]))
  }
  #Second quadrant
  if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == 1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
  }
  #Fourth quadrant
  if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == -1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + (2*pi)
  }
  #Third quadrant
  if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == -1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
  }
  #Save values in dataframe
  SZonedf$r[i] <- temp_r
  SZonedf$theta[i] <- temp_theta
}

#Violin ANOVA Plot
ump_ds_data <- readRDS(file_names2[1])
ind <- rep(1, length(ump_ds_data))
means <- array(mean(ump_ds_data), dim = c(94))
sds <- array(sd(ump_ds_data), dim = c(94))
ns <- array(length(ump_ds_data), dim = c(94))
for(u in 2:94){
  temp_data <- readRDS(file_names2[u])
  means[u] <- mean(temp_data)
  sds[u] <- sd(temp_data)
  ns[u] <- length(temp_data)
  ump_ds_data <- c(ump_ds_data, temp_data)
  ind <- c(ind, rep(u, length(temp_data)))
}

for(i in 1:length(ump_ds_data)){
  if(ump_ds_data[i] > 1.375){
    ump_ds_data[i] = 1.375
  }
}

# DF for comparing with umpscorecards AAx
df1 <- data.frame(UmpNames,
                  means)
UScsv <- read.csv('Data/UmpScorecards2023.csv')
df1$AAx <- UScsv$AAx
df1$Total_Pitches <- UScsv$PC
df1$AAxrank <- 0
df1$AAxrank[order(df1$AAx)] <- c(94:1)
df1 <- df1[order(df1$means),]
df1$DSrank <- c(1:94)
df1$UmpNames[93] <- 'David Arrieta*'
df1$UmpNames[94] <- 'Randy Rosenberg*'
colnames(df1) <- c('UmpNames', 'Mean DS', 'AAx', 'Total Pitches', 
                   'AAx Rank', 'DS Rank')
df1$`AAx Rank` <- as.integer(df1$`AAx Rank`)
df1 <- df1[,c(1,4, 6, 2, 3, 5)]

ui <- fluidPage(
  selectInput('UmpName', label = 'Dataset', choices = UmpNames),
  fluidRow(
    column(width = 4,  # First column takes 50% of the width
           plotOutput('plot1', height = "450")),  # First plot on the left
    column(width = 6,  # Second column takes 50% of the width
           plotOutput('plot2', height = "300px"),  # Second plot on top right
           plotOutput('plot3', height = "300px"))  # Third plot on bottom right
  ),
  tableOutput("table")
)


server <- function(input, output, session){
  dataset <- reactive({retreive_data(input$UmpName)})
  
  output$plot1 <- renderPlot({plot_EUZ1(dataset()$data, 
                                        dataset()$CI, 
                                        dataset()$name)})
  DSplots <- reactive({plotDS(dataset()$num)})
  output$plot2 <- renderPlot({DSplots()$plot1})
  output$plot3 <- renderPlot({DSplots()$plot2})
  
  output$table <- renderTable({df1})
}

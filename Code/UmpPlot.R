#Author: Adam Slivinsky
#Work: Evaluating MLB Umpires, Plotting Functions before Model Fitting
#Date: 11 November 2023

# This code just takes in an umpire-season of data, and plots
# called balls and strikes from the data.

# Good to double check the EUZ-inference matches these plots!

#Clear Data Environment
rm(list=ls(all=TRUE))
set.seed(7) 

#Load Libraries
library(rstan)
library(ggplot2)
library(mgcv)
library(boot)
library(MASS)
library(bayesplot)
library(BayesLogit)
library(pracma)
library(invgamma)
library(mvtnorm)
library(dplyr)
library(Matrix)
library(tidybayes)

#Load work(Laptop)
ump_data <- readRDS('Data/ump_seasons/Ump33_23.RDS')
ump_names <- readRDS('Data/ump_seasons/UmpNames.RDS')

#Filter to one game
#ump_data <- ump_data[ump_data$game_pk == 661832,]

#Filter out missing values
ump_data <- ump_data[which(is.na(ump_data$pfx_x) == FALSE),]
ump_data <- ump_data[which(is.na(ump_data$pfx_z) == FALSE),]

#Get strikes
called_strike_data <- ump_data[which(ump_data$description == 'called_strike'),]

#Get balls
ball_data <- ump_data[which(ump_data$description == 'ball'),]

#Combine called strikes and balls
data1 <- rbind(called_strike_data, ball_data)
data1$is_strike <- as.numeric(data1$description == 'called_strike')
data1$px <- data1$plate_x
data1$pz <- data1$plate_z

#Normalize pz
#Adjust for strike zone differences
for (i in 1:length(data1$sz_bot)){
  data1$pz[i] = 2*((data1$pz[i] - data1$sz_top[i])/ (data1$sz_top[i] - data1$sz_bot[i])) + 3.5
}

#Center pz
data1$pz <- data1$pz - 2.5
called_strike_data <- data1[which(data1$description == 'called_strike'),]

#Define strike zone dataframe with given MLB strike zone locations
SZonedf <- data.frame(x=c(rep(0.8308,200), rep(-0.8308,200), linspace(-0.8308, 0.8308, 200), linspace(-0.8308, 0.8308, 200)),
                      y = c(linspace(-1.1225, 1.1225, 200), linspace(-1.1225, 1.1225, 200), rep(-1.1225,200), rep(1.1225,200)),
                      r = rep(0,800), theta = rep(0,800))

#Get data and MLB strike_zone in polar coords
for(i in 1:length(SZonedf$x)){
  #Get radius of given point
  temp_r <- sqrt((SZonedf$x[i]**2) + (SZonedf$y[i]**2))
  
  #Get theta of given point
  #First quadrant
  if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == 1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i]))
  }
  #Second quadrant
  if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == 1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
  }
  #Fourth quadrant
  if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == -1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + (2*pi)
  }
  #Third quadrant
  if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == -1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
  }
  #Save values in dataframe
  SZonedf$r[i] <- temp_r
  SZonedf$theta[i] <- temp_theta
}

data1$r <- rep(0, length(data1$pz))
data1$theta <- rep(0, length(data1$pz))
for(i in 1:length(data1$pz)){
  data1$r[i] <- sqrt((data1$px[i])**2 + (data1$pz[i])**2)
  #First quadrant
  if((sign(data1$px[i]) == 1) && (sign(data1$pz[i]) == 1)){
    data1$theta[i] <- atan((data1$pz[i] / data1$px[i]))
  }
  #Second quadrant
  if((sign(data1$px[i]) == -1) && (sign(data1$pz[i]) == 1)){
    data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + pi
  }
  #Fourth quadrant
  if((sign(data1$px[i]) == 1) && (sign(data1$pz[i]) == -1)){
    data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + (2*pi)
  }
  #Third quadrant
  if((sign(data1$px[i]) == -1) && (sign(data1$pz[i]) == -1)){
    data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + pi
  }
}


#Randomize ball data (helps for plotting)
data1 <- data1[sample(1:nrow(data1)),]

#Plot strikes and balls
ggplot(data1, aes(x = px, y = pz, color = description)) + 
  geom_point(alpha = 0.04, cex=9) + ggtitle('David Rackley Called Pitches\nin 2023 MLB Season') + xlim(-1.35, 1.35) + ylim(-1.75,1.75) + 
  coord_fixed() + geom_point(data=data.frame(SZonedf), aes(x=x, y=y), color='black', cex=0.3) + 
  scale_color_manual(values=c('green', 'red')) + xlab('Horizontal distance from center of Strike Zone') + 
  ylab('Vertical distance from center of Strike Zone')

ggplot(data1, aes(x=theta, y = r, color = description)) + geom_point(alpha = 0.075, cex=10) + ggtitle('David Rackley Called Pitches in 2023 MLB Season') + 
  xlim(0, 2*pi) + ylim(0.5,1.75) + xlab(bquote(theta))+
  geom_point(data=data.frame(SZonedf), aes(x=theta, y=r), color='black', cex=0.3) + 
  scale_color_manual(values=c('green', 'red')) 

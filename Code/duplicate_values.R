#Author: Adam Slivinsky
#Work: Evaluating MLB Umpires, Implementing Gibbs Sampler
#Date: 28 March 2024

#This code investigated some duplicate values that interrupted the fitting of
#the Gibbs Sampler model. 
#Basically, there are some pitches within an umpire-season that have the exact
# same location. The majority of these actually differ in their outcome, 
# as in one will be a strike and the other will be a ball.
# The choice of what to do with these pitches is negligible, as long as one is 
# removed the model fits successfully. Either treating it as a ball or a strike
# does not affect model fitting significantly

#Clear Data Environment
rm(list=ls(all=TRUE))
set.seed(7) 

#Load Libraries
library(rstan)
library(ggplot2)
library(mgcv)
library(boot)
library(MASS)
library(bayesplot)
library(BayesLogit)
library(pracma)
library(invgamma)
library(mvtnorm)
library(dplyr)
library(Matrix)
library(tidybayes)

#MCMC Functions
sample_beta <- function(m_vec, y_vec, x_mat, w_vec, M_inv){
  k_vec = y_vec - 0.5*m_vec
  V_mat <- chol2inv(chol(as.matrix(t(x_mat)%*%Diagonal(length(w_vec),w_vec)%*%x_mat + M_inv)))
  u_vec <- V_mat %*% (t(x_mat) %*% k_vec)
  beta_temp <- rmvnorm(1, u_vec, V_mat, method='chol')
  return(beta_temp)
}


sample_w <- function(m_vec, beta, x_vec){
  temp_w <- rpg(num=length(m_vec), h=m_vec, x_vec %*% beta)
  return(temp_w)
}

sample_sigma <- function(a, b, beta_j){
  temp_a <- a + length(beta_j)/2
  temp_b <- b + (0.5 * t(beta_j) %*% beta_j)
  new_sig <- 1/rgamma(n=1, shape = temp_a, temp_b)
  return(new_sig)
}

binom_dev_resid <- function(y, u_hat, m_vec){
  temp1 <- array(0, dim = c(length(y)))
  for(i in 1:length(y)){
    if(u_hat[i] <= 0){
      u_hat[i] <- 0.001
    }
    if(u_hat[i] >= 1){
      u_hat[i] <- 0.999
    }
    sign1 <- sign(y[i] - u_hat[i])
    temp1[i] <- sign1 * ((2*(y[i] * log(y[i] / u_hat[i]) + 
                               (m_vec[i] - y[i])*log((m_vec[i] - y[i])/(m_vec[i] - u_hat[i]))))**(1/2))
    if(y[i] == m_vec[i]){
      temp1[i] <- sign1 * ((2*(y[i] * log(y[i] / u_hat[i])))**(1/2))
    }
    if(y[i] == 0){
      temp1[i] <- sign1 * ((2*((m_vec[i])*log((m_vec[i])/(m_vec[i] - u_hat[i]))))**(1/2))
    }
  }
  return(temp1)
}

binom_dev <- function(y, u_hat, m_vec){
  dev_temp <- 0
  for(i in 1:length(y)){
    if(y[i] == m_vec[i]){
      dev_temp <- dev_temp + (y[i] * log(y[i] / u_hat[i]))
    }
    if(y[i] == 0){
      dev_temp <- dev_temp + (m_vec[i] - y[i])*log((m_vec[i] - y[i])/(m_vec[i] - u_hat[i]))
    }
  }
  return(dev_temp)
}

binom_log_loss <- function(y, u_hat){
  ll_temp <- 0
  for(i in 1:length(y)){
    if(u_hat[i] <= 0){
      u_hat[i] <- 0.001
    }
    if(u_hat[i] >= 1){
      u_hat[i] <- 0.999
    }
    ll_temp <- ll_temp - (y[i] * log(u_hat[i] / (1 - u_hat[i])) + log(1 - u_hat[i]))
  }
  return(ll_temp)
}

maxU <- 94
startU <- 1
endU <- 94
file_names <- paste0('Data/ump_seasons/Ump', seq(1, maxU, by = 1), '_23.RDS')

num_dupes <- array(0, dim = c(4,maxU))
if_dupes <- array(0, dim = c(4,maxU))
dupes <- array(0, dim =c(6,3, 94))
dupes2 <- array(0, dim =c(6,3, 94))

for(um in startU:endU){
  ump_data <- readRDS(file_names[um])
  weights <- readRDS('RDS/GLM/weights.rds')
  biases <- readRDS('RDS/GLM/biases.rds')
  freq_coef <- readRDS('RDS/FreqCoef.rds')
  
  #Filter out missing values
  ump_data <- ump_data[which(is.na(ump_data$pfx_x) == FALSE),]
  ump_data <- ump_data[which(is.na(ump_data$pfx_z) == FALSE),]
  
  #Get strikes
  called_strike_data <- ump_data[which(ump_data$description == 'called_strike'),]
  
  #Get balls
  ball_data <- ump_data[which(ump_data$description == 'ball'),]
  
  #Combine called strikes and balls
  data1 <- rbind(called_strike_data, ball_data)
  data1$is_strike <- as.numeric(data1$description == 'called_strike')
  data1$px <- data1$plate_x
  data1$pz <- data1$plate_z
  
  #Normalize pz
  #Adjust for strike zone differences
  for (i in 1:length(data1$sz_bot)){
    data1$pz[i] = 2*((data1$pz[i] - data1$sz_top[i])/ (data1$sz_top[i] - data1$sz_bot[i])) + 3.5
  }
  
  #Center pz
  data1$pz <- data1$pz - 2.5
  called_strike_data <- data1[which(data1$description == 'called_strike'),]
  
  SZonedf <- data.frame(x=c(rep(0.8308,200), rep(-0.8308,200), linspace(-0.8308, 0.8308, 200), linspace(-0.8308, 0.8308, 200)),
                        y = c(linspace(-1.1225, 1.1225, 200), linspace(-1.1225, 1.1225, 200), rep(-1.1225,200), rep(1.1225,200)),
                        r = rep(0,800), theta = rep(0,800))
  for(i in 1:length(SZonedf$x)){
    #Get radius of given point
    temp_r <- sqrt((SZonedf$x[i]**2) + (SZonedf$y[i]**2))
    
    #Get theta of given point
    #First quadrant
    if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == 1)){
      temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i]))
    }
    #Second quadrant
    if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == 1)){
      temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
    }
    #Fourth quadrant
    if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == -1)){
      temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + (2*pi)
    }
    #Third quadrant
    if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == -1)){
      temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
    }
    #Save values in dataframe
    SZonedf$r[i] <- temp_r
    SZonedf$theta[i] <- temp_theta
  }
  #Clear memory for unused variables
  rm(ball_data)
  rm(called_strike_data)
  rm(ump_data)
  
  
  #Change to polar coordinates (shift pz down by 2.5)
  data1$r <- rep(0, length(data1$pz))
  data1$theta <- rep(0, length(data1$pz))
  for(i in 1:length(data1$pz)){
    data1$r[i] <- sqrt((data1$px[i])**2 + (data1$pz[i])**2)
    #First quadrant
    if((sign(data1$px[i]) == 1) && (sign(data1$pz[i]) == 1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i]))
    }
    #Second quadrant
    if((sign(data1$px[i]) == -1) && (sign(data1$pz[i]) == 1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + pi
    }
    #Fourth quadrant
    if((sign(data1$px[i]) == 1) && (sign(data1$pz[i]) == -1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + (2*pi)
    }
    #Third quadrant
    if((sign(data1$px[i]) == -1) && (sign(data1$pz[i]) == -1)){
      data1$theta[i] <- atan((data1$pz[i] / data1$px[i])) + pi
    }
  }
  
  #Change stand to a numeric
  data1 <- data1[,c(95, 98, 99)]
  
  #Get number of duplicated values
  num_dupes[1,um] <- sum(duplicated(data1[,2:3]))
  num_dupes[2,um] <- sum(duplicated(data1))
  
  #If dupes exist, get index
  if(sum(duplicated(data1[,2:3])) > 0){
    if_dupes[1,um] <- 1
  }
  if(sum(duplicated(data1)) > 0){
    if_dupes[2,um] <- 1
  }
  
  bb <- as.matrix(data1[duplicated(data1[,2:3]),])
  if(length(bb) > 0){
    dupes[1:length(bb[,1]), , um] <- (bb)
    
  }
  bb <- as.matrix(data1[duplicated(data1),])
  if(length(bb) > 0){
    dupes2[1:length(bb[,1]), , um] <- (bb)
    
  }

  # #Set up ELM output hidden and output layer
  # nodes <- 200
  # complex_param <- 3
  # output_layerP1 <- array(0, dim = c(nodes, length(data1$r)))
  # for(i in 1:length(data1$r)){
  #   for(h in 1:nodes){
  #     ind_temp = h %% (nodes/2)
  #     if(ind_temp == 0){
  #       ind_temp = (nodes/2)
  #     }
  #     h_temp <- sigmoid(weights[ind_temp,1] * cos(data1$theta[i] + weights[ind_temp,2]) + biases[ind_temp])
  #     if(h <= (nodes/2)){
  #       output_layerP1[h,i] <- h_temp 
  #     }
  #     if(h > (nodes/2)){
  #       output_layerP1[h,i] <- h_temp * data1$r[i]
  #     }
  #   }
  # }
  # 
  # hidden_layerP <- data.frame(data1$is_strike, t(output_layerP1))
  # rm(output_layerP1)
  # 
  # #Check for missing values!!
  # sum(is.na(hidden_layerP$X2))
  # 
  # #Remove missing values from hidden layer in polar coords
  # hidden_layerP <- hidden_layerP %>% filter(!is.na(X1))
  # 
  # #Get number of duplicated values
  # num_dupes[3,um] <- sum(duplicated(hidden_layerP[,2:201]))
  # num_dupes[4,um] <- sum(duplicated(hidden_layerP))
  # 
  # #If dupes exist, get index
  # if(sum(duplicated(hidden_layerP[,2:201])) > 0){
  #   if_dupes[3,um] <- 1
  # }
  # if(sum(duplicated(hidden_layerP)) > 0){
  #   if_dupes[4,um] <- 1
  # }
}

dupes - dupes2

#Author: Adam Slivinsky
#Work: Pulling an umpire-season of data from MLB database
#Date: 26 September 2023

# This code takes in the entire dataset of all called pitches in 2023,
# And creates separate datasets for each individual umpire-season, saving
# them in the /Data folder. 

#Clear Data Environment
rm(list=ls(all=TRUE))
set.seed(7) 

#Load in season info
ump_data <- read.csv('Data/Umpire_season_23.csv')

ump_counts <- (table(ump_data$umpire))
umps <- dimnames(ump_counts)[[1]]
ump_games <- as.vector(ump_counts)
file_names <- paste0('Data/ump_seasons/Ump', seq(1, length(umps), by = 1), '_23.RDS')

for(u in 1:length(umps)){
  temp_data <- ump_data[which(ump_data$umpire == umps[u]),]
  saveRDS(temp_data, file_names[u])
}

saveRDS(umps, 'Data/ump_seasons/UmpNames.RDS')


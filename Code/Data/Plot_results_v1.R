#Author: Adam Slivinsky
#Work: HW 3 Problem 2 Code
#Date: 11 November 2023

#Clear Data Environment
rm(list=ls(all=TRUE))
set.seed(7) 

#Load Libraries
library(rstan)
library(ggplot2)
library(mgcv)
library(boot)
library(MASS)
library(bayesplot)
library(BayesLogit)
library(pracma)
library(invgamma)
library(mvtnorm)
library(dplyr)
library(Matrix)
library(tidybayes)

weights <- readRDS('RDS/GLM/weights.rds')
biases <- readRDS('RDS/GLM/biases.rds')
freq_coef <- readRDS('RDS/FreqCoef.rds')

maxU <- 94
startU <- 1
endU <- 3
file_names <- paste0('Data/ump_seasons/Ump', seq(1, maxU, by = 1), '_23.RDS')
file_names2 <- paste0('DS_RDS/Ump', seq(1, maxU, by = 1), 'DS.RDS')
file_names3 <- paste0('Beta_RDS/Ump', seq(1, maxU, by = 1), 'Beta.RDS')
file_names4 <- paste0('EUZ_RDS/Ump', seq(1, maxU, by = 1), 'EUZ.RDS')

LD_theta <- seq(0, (2*pi), by=0.005)
nodes <- 200


um = 3
ump_DS <- readRDS(file_names2[um])
post_beta_mean <- readRDS(file_names3[um])
CI_LD_r <- array(readRDS(file_names4[um]), dim = c(length(LD_theta), 3, 6))[,,6]

#Model prediction
#Define number of linearly spaced points to predict over
num_points <- 100
pred_mat <- NULL
pred_mat$px <- linspace(-2, 2, num_points)
pred_mat$pz <- linspace(-2.5, 2.5, num_points)
pred_mat$is_strike_pred_L2 = array(0, dim = c(num_points, num_points))
#List to store pairs of linearly spaced points 
new_data <- NULL
new_data$px <- array(0, dim = c(num_points * num_points))
new_data$pz <- array(0, dim = c(num_points * num_points))
new_output_layerPL <- array(0, dim = c(nodes, (num_points * num_points)))
#Construct hidden layer, vector notation
for(n1 in 1:num_points){
  for(n2 in 1:num_points){
    new_data$px[(num_points)*(n1 - 1) + n2] <- pred_mat$px[n1]
    new_data$pz[(num_points)*(n1 - 1) + n2] <- pred_mat$pz[n2]
    
    #Get radius of given point
    temp_r <- abs(sqrt((pred_mat$px[n1]**2) + (pred_mat$pz[n2]**2)) + 0.12)
    
    #Get theta of given point
    #First quadrant
    if((sign(pred_mat$px[n1]) == 1) && (sign(pred_mat$pz[n2]) == 1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1]))
    }
    #Second quadrant
    if((sign(pred_mat$px[n1]) == -1) && (sign(pred_mat$pz[n2]) == 1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1])) + pi
    }
    #Fourth quadrant
    if((sign(pred_mat$px[n1]) == 1) && (sign(pred_mat$pz[n2]) == -1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1])) + (2*pi)
    }
    #Third quadrant
    if((sign(pred_mat$px[n1]) == -1) && (sign(pred_mat$pz[n2]) == -1)){
      temp_theta <- atan((pred_mat$pz[n2] / pred_mat$px[n1])) + pi
    }
    #Construct hidden layer 
    for(h in 1:nodes){
      ind_temp = h %% 100
      if(ind_temp == 0){
        ind_temp = 100
      }
      h_temp <- sigmoid(weights[ind_temp,1] * cos(temp_theta + weights[ind_temp,2]) + biases[ind_temp])
      if(h <= 100){
        new_output_layerPL[h,((num_points)*(n1 - 1) + n2)] <- h_temp * temp_r
      }
      if(h > 100){
        new_output_layerPL[h,((num_points)*(n1 - 1) + n2)] <- h_temp
      }
    }
  }
}

#Get predictions from posterior beta across observation window (vector notation)
new_data$is_strike_pred_L2 <- array(0, dim=c(num_points * num_points))
for(j in 1:length(new_data$is_strike_pred_L2)){
  temp_eta <- new_output_layerPL[,j] %*% post_beta_mean
  pi_j <- exp(temp_eta)/ ( 1 + exp(temp_eta))
  new_data$is_strike_pred_L2[j] <- pi_j
}

#Transform predictions from vector to matrix notation
for(n1 in 1:num_points){
  for(n2 in 1:num_points){
   pred_mat$is_strike_pred_L2[n1,n2] <- new_data$is_strike_pred_L2[(num_points)*(n1 - 1) + n2]
  }
}



#Plotting parameters
colfunc <- colorRampPalette(c('green', 'white', 'red'))
SZone <- NULL
SZone$x <- rbind(rep(0.7083,200), rep(-0.7083,200), linspace(-0.7083, 0.7083, 200), linspace(-0.7083, 0.7083, 200))
SZone$y <- rbind(linspace(-1, 1, 200), linspace(-1, 1, 200), rep(-1,200), rep(1,200))

#Plot ESZ (No contour line)
filled.contour(pred_mat$px, pred_mat$pz, pred_mat$is_strike_pred_L2, xlim=c(-1.2, 1.2), ylim=c(-1.5, 1.5), 
               levels = seq(0, 1, l=11), asp = 1, col = colfunc(11), main = 'ESZ for Angel Hernandez (Polar CS)', 
               plot.axes = { axis(1); axis(2); points(SZone$x, SZone$y, pch = 20, cex = 0.5) })

#Transform MLB Strike zone from Cartesian Space to Polar Coordinates
#Define strike zone dataframe with given MLB strike zone locations
SZonedf <- data.frame(x=c(rep(0.7083,200), rep(-0.7083,200), linspace(-0.7083, 0.7083, 200), linspace(-0.7083, 0.7083, 200)),
                      y = c(linspace(-1, 1, 200), linspace(-1, 1, 200), rep(-1,200), rep(1,200)),
                      r = rep(0,800), theta = rep(0,800))
for(i in 1:length(SZonedf$x)){
  #Get radius of given point
  temp_r <- sqrt((SZonedf$x[i]**2) + (SZonedf$y[i]**2))
  
  #Get theta of given point
  #First quadrant
  if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == 1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i]))
  }
  #Second quadrant
  if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == 1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
  }
  #Fourth quadrant
  if((sign(SZonedf$x[i]) == 1) && (sign(SZonedf$y[i]) == -1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + (2*pi)
  }
  #Third quadrant
  if((sign(SZonedf$x[i]) == -1) && (sign(SZonedf$y[i]) == -1)){
    temp_theta <- atan((SZonedf$y[i] / SZonedf$x[i])) + pi
  }
  #Save values in dataframe
  SZonedf$r[i] <- temp_r
  SZonedf$theta[i] <- temp_theta
}

#Plot r vs theta for the 50% Contour Line in Polar Coordinates
#ALSO Plot confidence interval for the Contour Line
#For average of all 
ggplot(data.frame(r=CI_LD_r[,2], theta=LD_theta), aes(theta, r)) + 
  geom_point(cex = 1, color='red') + ylim(0.65,1.275) + 
  ggtitle('Plot of 50% ESZ r as a function of theta\nwith Confidence Intervals') + 
  geom_point(data=SZonedf, aes(x=theta, y=r), cex=1, color='black') + 
  geom_ribbon(data = data.frame(r=CI_LD_r[,2], lower=CI_LD_r[,1],
                                upper=CI_LD_r[,3], theta=LD_theta),
              aes(x=LD_theta, ymin=lower, ymax=upper), 
              color='red', alpha=0.5) + xlab(bquote(theta))



#Get average of contour line in polar coordinates to cartesian space
LD_x_m <- (CI_LD_r[,2] * cos(LD_theta))
LD_x_CIL_m <- (CI_LD_r[,1] * cos(LD_theta))
LD_x_CIU_m <- (CI_LD_r[,3] * cos(LD_theta))
LD_y_m <- (CI_LD_r[,2] * sin(LD_theta))
LD_y_CIL_m <- (CI_LD_r[,1] * sin(LD_theta))
LD_y_CIU_m <- (CI_LD_r[,3] * sin(LD_theta))

#BEST PLOT 3
filled.contour(pred_mat$px, pred_mat$pz, pred_mat$is_strike_pred_L2, xlim = c(-1.2, 1.2), ylim = c(-1.5, 1.5), 
               levels = seq(0, 1, l=11), asp = 1, col = colfunc(11), main = 'Neural Network Probability Surface', 
               plot.axes = { axis(1); axis(2); points(c(SZone$x, LD_x_m),
                                                      c(SZone$y, LD_y_m),
                                                      pch = 20, cex = 0.3) })

#BEST PLOT 2
ggplot() + 
  geom_point(alpha = 0.05, cex=10) + ggtitle('NN Contour Lines and 95% CI bands') + xlim(-1.2, 1.2) + ylim(-1.5, 1.5) + 
  coord_fixed() + geom_point(data=data.frame(SZonedf), aes(x=x, y=y), color='black', cex=0.3) + 
  scale_color_manual(values=c('green', 'red')) + xlab('Horizontal distance from center of Strike Zone') + 
  ylab('Vertical distance from center of Strike Zone') + 
  geom_point(data = data.frame(x = c(LD_x_m, LD_x_CIL_m, LD_x_CIU_m),
                               y = c(LD_y_m, LD_y_CIL_m, LD_y_CIU_m),
                               description = rep(NA, length(c(LD_x_m, LD_x_CIL_m, LD_x_CIU_m)))),
             aes(x = x, y = y), cex = 0.3, color = 'black')

#Best Plot 22 
ggplot(data1, aes(x = px, y = pz, color = description)) + 
  geom_point(alpha = 0.05, cex=10) + ggtitle('NN Contour Lines and 95% CI bands') + xlim(-1.2, 1.2) + ylim(-1.5, 1.5) + 
  coord_fixed() + geom_point(data=data.frame(SZonedf), aes(x=x, y=y), color='black', cex=0.3) + 
  scale_color_manual(values=c('green', 'red')) + xlab('Horizontal distance from center of Strike Zone') + 
  ylab('Vertical distance from center of Strike Zone') + 
  geom_point(data = data.frame(x = LD_x_m,
                               y = LD_y_m,
                               description = rep(NA, length(LD_x_m))),
             aes(x = x, y = y), cex = 0.75, color = 'black') + 
  geom_point(data = data.frame(x = LD_x_CIL_m,
                               y = LD_y_CIL_m,
                               description = rep(NA, length(LD_x_CIL_m))),
             aes(x = x, y = y), cex = 0.1, color = 'blue') + 
  geom_point(data = data.frame(x = LD_x_CIU_m,
                               y = LD_y_CIU_m,
                               description = rep(NA, length(LD_x_CIU_m))),
             aes(x = x, y = y), cex = 0.1, color = 'blue')



#Plot 50% contour line, 

#Get score of accuracy
Riem_Sum_acc <- array(0, dim=c(length(LD_theta) - 1, n_folds))
Riem_Sum_cons <- array(0, dim=c(length(LD_theta) - 1, n_folds))
MLB_r2 <- c(0.803, SZonedf$r[order(SZonedf$theta)], 0.803)
MLB_theta2 <- c(0, SZonedf$theta[order(SZonedf$theta)], 2*pi)
MLB_theta <- LD_theta[1:(length(LD_theta) -1)]
MLB_r <- interp1(MLB_theta2, MLB_r2, 
                 MLB_theta)
for(nf in 1:n_folds){
  for(i in 1:length(MLB_theta)){
    Riem_Sum_acc[i, nf] <- ((2*pi / length(MLB_theta)) * 
                              (abs(MLB_r[i] - CI_LD_r[i,2,nf])))
    Riem_Sum_cons[i, nf] <- ((2*pi / length(MLB_theta)) * 
                               (abs(CI_LD_r[i,1,nf] - CI_LD_r[i,3,nf])))
  }
}
ump_acc <- c(colSums(Riem_Sum_acc), mean(colSums(Riem_Sum_acc)))
ump_cons <- c(colSums(Riem_Sum_cons), mean(colSums(Riem_Sum_cons)))
#Plot log loss for Cart and Polar
barplot(c(ump_acc,ump_cons), main = 'Umpire Accurary and Consistency across Folds',
        names.arg=c(paste0('Acc_fold', seq(1, n_folds, by = 1)), 'Ave_Acc',
                    paste0('Cons_fold', seq(1, n_folds, by = 1)), 'Ave_Cons'),
        ylim=c(0,0.75))



#mean and std dev of departure score
c(mean(as.vector(sims1[,,,(nodes + 3)])),
  sd(as.vector(sims1[,,,(nodes + 3)])))
#Density of departure scores
ggplot(data=data.frame(x = as.vector(sims1[,,,(nodes +3)])),
       aes(x = x)) + geom_density() +
  labs(title ='Departure Score for Umpire A') + 
  xlab('Departure Score') + ylab('Density')


ggplot(data=data.frame(x = bb1),
       aes(x = x)) + geom_density() +
  labs(title ='Departure Score2 for Umpire A') + 
  xlab('Departure Score') + ylab('Density')